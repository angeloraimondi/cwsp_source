def set(scope, target, cw):
	if(target == "CWLITEARM" or "CW308" in target):
		scope.gain.gain = 25
		scope.adc.samples = 5000
		scope.adc.offset = 0
		scope.adc.basic_mode = "rising_edge"
		scope.clock.clkgen_freq = 7370000
		scope.clock.adc_src = "clkgen_x4"
		scope.trigger.triggers = "tio4"
		scope.io.tio1 = "serial_rx"
		scope.io.tio2 = "serial_tx"
		scope.io.hs2 = "clkgen"
		
	elif(target == 	'NUCLEO'):
		scope.gain.db = 25
		scope.adc.samples = 5000
		scope.adc.offset = 0
		scope.adc.basic_mode = "rising_edge"
		scope.clock.clkgen_freq = 8000000
		scope.clock.adc_src = "clkgen_x4"
		scope.trigger.triggers = "tio4"
		scope.io.tio1 = "serial_tx"
		scope.io.tio2 = "serial_rx"
	
	if("CW" in target):
		programmer = cw.capture.api.programmers.STM32FProgrammer()
		programmer.scope = scope
		programmer._logging = None
		programmer.open()
		programmer.find()
		programmer.close()
